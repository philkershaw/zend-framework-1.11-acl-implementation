<?php
/**
 * Flash Message View Helper
 * Determines the type and passes back the appropriate HTML. Accespts arrays and strings
 * 
 * @author Phil Kershaw
 */
class Zend_View_Helper_FlashMessage
{
	/**
	 * The formatted flash message
	 * 
	 * @var string
	 */
	protected $_flashMessage = null;

	/**
	 * Flash Message
 	 * Determines the type and passes back the appropriate HTML. Accespts arrays and strings
 	 * 
 	 * @param array/string $messages
 	 * @return string $this->_flashMessages
 	 */
	public function FlashMessage($messages)
	{
		if ($messages)
    {
			switch (is_array($messages))
			{
				case True:
					foreach($messages as $message)
					{
						if ($message['type'] == 'error'){
							$this->_flashMessage = <<<message
							<div class="alert alert_red"><img height="24" width="24" src="/images/icons/small/white/alert.png" />{$message['message']}</div>
message;
						}
						else
						{
							$this->_flashMessage = <<<message
							<div class="alert alert_green"><img height="24" width="24" src="/images/icons/small/white/alert_2.png" />{$message['message']}</div>
message;
						}
					}
				break;
				default:
					$this->_flashMessage = <<<message
          <div class="alert alert_orange"><img height="24" width="24" src="/images/icons/small/white/alert_2.png" />{$messages}</div>
message;
				break;
			}
    }
    return $this->_flashMessage;
	}
}