<?php
class Auth_Model_DbTable_Users extends Zend_Db_Table_Abstract
{
	protected $_name = 'users';
	protected $_primary = 'id';

	/**
	 * Get All Users
	 * Fetches all user data for all users in the Db
	 * 
	 * @return array all users
	 */
	public function getAll()
	{
		return $this->fetchAll()->toArray();
	}
	/**
	 * Get User
	 * Fetches all user data for requested user in the Db
	 * 
	 * @return array requested user details
	 */
	public function get($id)
	{	
		$select = $this->select()
					   ->from($this, array('id','username','role'))
					   ->where("id = {$id}");
		
		return $this->fetchRow($select)->toArray();
	}
	/**
	 * Update User
	 * Updates a user record with given data
	 * 
	 * @param int $id
	 * @param array $data
	 * @return mixed
	 */
	public function updateUser($id, $data)
	{
		$this->update($data, "id = {$id}");
	}
	
	public function add($data)
	{
		$row = $this->fetchRow(
				$this->select()
				     ->where("username = ?", $data['username'])
				);
		
		if (!$row){
			if ($this->insert($data))
				return TRUE;
		}
		return FALSE;
	}
}