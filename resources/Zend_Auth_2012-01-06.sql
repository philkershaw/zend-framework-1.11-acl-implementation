# ************************************************************
# Sequel Pro SQL dump
# Version 3408
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.9)
# Database: Zend_Auth
# Generation Time: 2012-01-06 16:59:28 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table resources
# ------------------------------------------------------------

DROP TABLE IF EXISTS `resources`;

CREATE TABLE `resources` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `module` varchar(128) NOT NULL DEFAULT '',
  `controller` varchar(128) NOT NULL DEFAULT '',
  `action` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `resources` WRITE;
/*!40000 ALTER TABLE `resources` DISABLE KEYS */;

INSERT INTO `resources` (`id`, `module`, `controller`, `action`)
VALUES
	(00000000009,'admin','index','index'),
	(00000000010,'admin','index','updateresources'),
	(00000000017,'admin','index','permissions'),
	(00000000018,'auth','index','index'),
	(00000000019,'auth','index','index-two'),
	(00000000020,'auth','index','index-three'),
	(00000000021,'auth','index','index-four'),
	(00000000022,'auth','index','logout'),
	(00000000023,'default','error','error'),
	(00000000024,'default','index','index');

/*!40000 ALTER TABLE `resources` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_resources
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_resources`;

CREATE TABLE `user_resources` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned zerofill NOT NULL DEFAULT '00000000000',
  `resource_id` int(11) unsigned zerofill NOT NULL DEFAULT '00000000000',
  `permission` enum('deny','allow') NOT NULL DEFAULT 'deny',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user_resources` WRITE;
/*!40000 ALTER TABLE `user_resources` DISABLE KEYS */;

INSERT INTO `user_resources` (`id`, `user_id`, `resource_id`, `permission`)
VALUES
	(00000000001,00000000001,00000000009,'allow'),
	(00000000002,00000000001,00000000010,'deny'),
	(00000000003,00000000002,00000000009,'deny'),
	(00000000004,00000000002,00000000010,'deny'),
	(00000000005,00000000003,00000000009,'allow'),
	(00000000006,00000000001,00000000017,'allow'),
	(00000000007,00000000001,00000000018,'allow'),
	(00000000008,00000000001,00000000019,'allow'),
	(00000000009,00000000001,00000000020,'allow'),
	(00000000010,00000000001,00000000021,'allow'),
	(00000000011,00000000001,00000000022,'allow'),
	(00000000013,00000000001,00000000023,'allow'),
	(00000000014,00000000001,00000000024,'allow');

/*!40000 ALTER TABLE `user_resources` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(128) NOT NULL DEFAULT '',
  `role` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `Username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `password`, `role`)
VALUES
	(0000000001,'phil','400ede500033a14f53b8effb2895bf7f04ff2903',''),
	(0000000002,'john','','');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
