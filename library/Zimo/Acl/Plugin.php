<?php

class Zimo_Acl_Plugin extends Zend_Controller_Plugin_Abstract
{
	/**
	 * Acl
	 * Zend ACL object passed to this class upon instantiation
	 * 
	 * @var Zend_Acl object
	 */
	private $_acl = null;
	/**
	 * Role
	 * Usually the username of the logged in user
	 * 
	 * @var string
	 */
	private $_role;

	protected $_redirector = null;
	protected $_flashMessenger;
     
    public function __construct()
    {
        $this->_initACL();
        $this->_redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
        $this->_flashMessenger = Zend_Controller_Action_HelperBroker::getStaticHelper('flashMessenger');
    }
    
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {   
        $role = ($this->_role) ? $this->_role : null;
        $module = $request->getModuleName();
        $controller = $request->getControllerName();
        $action = $request->getActionName();
        $resource = "{$module}-{$controller}-{$action}";

        if(!$this->_acl->isAllowed($this->_role, $resource)) 
        {  
          // $this->_flashMessenger->addMessage(array('type' => 'error', 'message' => "Permission denied."));
          // $this->_redirector->gotoUrl('/login');
          $request->setModuleName('auth')
                  ->setControllerName('index') 
                  ->setActionName('index');

        }
    }

    private function _initACL()
    {   
        $this->_acl = new Zend_Acl();
        $auth = Zend_Auth::getInstance();
        $this->_role = ($auth->hasIdentity()) ? $auth->getIdentity()->username : null;
        $this->_acl->addRole(new Zend_Acl_Role($this->_role));

        //Add resources from Db
        $resourceTable = new Auth_Model_DbTable_Resources();
        $permissionsTable = new Auth_Model_DbTable_Permissions();
        $resources = $resourceTable->fetchAll()->toArray();
        
        foreach ($resources as $resource){
        	$resourceToken = "{$resource['module']}-{$resource['controller']}-{$resource['action']}";

            $this->_acl->add(new Zend_Acl_Resource($resourceToken));

            if ($auth->getIdentity())
	        {
	        	$userPermissions = $permissionsTable->getUserPermissions($auth->getIdentity()->id);
	            foreach ($userPermissions as $permission)
	            {
	            	if ($resource['id'] == $permission['resource_id'])
	            	{
	            		switch ($permission['permission'])
	            		{
	            			case 'allow':
	            				$this->_acl->allow($this->_role, $resourceToken);
	            				break;
	            			case 'deny':
	            			default:
	            				$this->_acl->deny($this->_role, $resourceToken);
	            				break;
	            		}
	            		break;
	            	}
	        		else
	        		{
	        			$this->_acl->deny($this->_role, $resourceToken);
	            	}
	            }
	        }	
        }
    }
}