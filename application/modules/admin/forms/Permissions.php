<?php

class Admin_Form_Permissions extends Zend_Form
{
	/**
	 * User ID
	 * The user ID for whom to display current permissions
	 * 
	 * @var int
	 */
	public $_userID;

	public function __construct($userID, $_attribs)
	{
		$this->_userID = $userID;
		parent::__construct($_attribs);
	}

	public function init()
	{
		$resourcesTbl = new Auth_Model_DbTable_Resources();
		$resources = $resourcesTbl->fetchResources();
		$userResources = $resourcesTbl->fetchUserResources($this->_userID);

		$this->addElement(
			'hidden',
			'user',
			array(
				'value' => $this->_userID
				));

		foreach ($resources as $resource)
		{
			foreach ($userResources as $userResource)
			{
				if ($userResource['id'] == $resource['id'])
				{
					$permission = $userResource['permission'];
					break;
				}
			}

			$this->addElement(
				'radio',
				"{$resource['id']}",
				array(
	            'label'      => "{$resource['module']} / {$resource['controller']} / {$resource['action']}",
	            'required'   => true,
	            'attribs' =>    array(
	                                'id'=>"{$resource['module']}_{$resource['controller']}_{$resource['action']}",
	                            ),
	            'multioptions'   => array(
	                            'deny' => 'Deny',
	                            'allow' => 'Allow',
	                            	),
	            'value' => (isset($permission)) ? $permission : 'deny'
	        	)
	        );
		}
		$this->addElement(
			'button',
			'save',
			array(
				'class' => 'button_colour',
              	'label' => '<img height="24" width="24" alt="Bended Arrow Right" src="/images/icons/small/white/bended_arrow_right.png"><span>Save Permissions</span>',
              	'escape'=> false,
              	'type'  => 'submit',
              	'decorators' => array('ViewHelper'),
           		)
			);
		$this->setElementDecorators(array(
		    'ViewHelper',
		    array('Description', array('tag' => 'span')),
		    array('Errors'),
		    array('HtmlTag', array('tag' => 'td')),
		    array('Label', array('tag' => 'td')),
		    array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
		));
		$this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'static')),
            'Form',
            'Errors'
	        ));
	}
}