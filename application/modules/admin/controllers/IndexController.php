<?php

class Admin_IndexController extends Zend_Controller_Action
{
	protected $_redirector = null;
 	/**
 	 * Init
 	 * Sets up a local object for the redirector helper and ansures flash messages are displayed where used within actions.
 	 */
    public function init()
    {
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->view->message = $this->_helper->flashMessenger->getMessages();
    }

	public function indexAction()
	{
		
	}
	/**
	 * Update Resources
	 * Invokes the Zimo ACL Resources class in order to find all resources then inserts them to into the Db
	 * 
	 * @return array flash message
	 */
	public function updateresourcesAction()
	{
		try
		{
			$acl 	   = new Zimo_Acl_Resources();
			$resources = $acl->resources;

			$resourcesTable = new Auth_Model_DbTable_Resources();
			$resourcesTable->addResources($resources);	
			$this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Resources updated successfully."));
		}
		catch (Exception $e)
		{
			$this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Failed to update resources. The following error occurred: {$e->getMessage()}"));
		}
		$this->_redirector->gotoUrl('/admin');
	}
	/**
	 * Permissions
	 * Displays a form for managing user permissions to resources
	 */
	public function permissionsAction()
	{
        if ($this->_request->isPost())
        {
        	switch ($this->_request->getParam('crud'))
        	{
        		case 'update':
        			$post = $this->_request->getPost();
		            unset($post['save']);
		            $permissionsTbl = new Auth_Model_DbTable_Permissions();
		            try
		            {
		                $permissionsTbl->savePermissions($post);
		                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Permissions updated successfully."));
		            }
		            catch (Exception $e)
		            {
		                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "There was a problem saving the permissions. The following error occurred {$e->getMessage()}"));
		            }
		            $this->_redirector->gotoUrl('/admin/permissions');		
        			break;
        		default:
        			$user = $_POST['users'];
        			break;
        	}
        }
        $userForm = new Admin_Form_Users(array(
            'action' => '/admin/permissions',
            'method' => 'post',
            ));
        
        $userTbl = new Auth_Model_DbTable_Users();
		$users = $this->reformatArray($userTbl->getAll());
		if (!isset($user))
		{
			$userID = key($users);	
		}
		else
		{
			$userID = $user;
		}
		$userSelector = $userForm->getElement('users');
		$userSelector->setValue($userID)
				 	 ->setMultiOptions($users);

        $permissionsForm = new Admin_Form_Permissions($userID, array(
            'action' => '/admin/permissions/update',
            'method' => 'post',
            ));
        $this->view->form = $permissionsForm;
        $this->view->users = $userForm;
	}
	/**
	 * Reformat Array
	 * Arranges the array in a format ready for use with a Zend Select element
	 * 
	 * @return array reformatted data
	 */
	private static function reformatArray(Array $users)
	{
		$newArray = array();
		foreach ($users as $user)
		{
			$newArray[$user['id']] = $user['username'];
		}
		return $newArray;
	}
}