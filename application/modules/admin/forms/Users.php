<?php

class Admin_Form_Users extends Zend_Form
{
	public function init()
	{
		$this->addElement(
			'select',
			'users',
			array(
				'onChange'	   => 'this.form.submit()'
				)
			);
	}
}