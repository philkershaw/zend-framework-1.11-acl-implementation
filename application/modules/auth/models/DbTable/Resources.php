<?php

class Auth_Model_DbTable_Resources extends Zend_Db_Table_Abstract
{
	protected $_name 	= "resources";
	protected $_primary = "id";
	/**
	 * Add Resources
	 * Add the resources to the table
	 * 
	 * @param array $resources
	 */
	public function addResources($resources)
	{
		foreach ($resources as $module => $controllers)
		{
			foreach ($controllers as $controller => $actions)
			{
				foreach ($actions as $action)
				{
					$row = $this->fetchRow(
						$this->select()
							 ->where('module = ?', $module)
							 ->where('controller = ?', $controller)
							 ->where('action = ?', $action)
						);

					if (!$row)
					{
						$insert = array(
							'module' 	 => $module,
							'controller' => $controller,
							'action'	 => $action
							);
						$this->insert($insert);	
					}
				}
			}
		}
	}
	/**
	 * Fetch Resources
	 * Fetches the resources from the Db and returns an array
	 * 
	 * @return array
	 */
	 public function fetchResources()
	 {
	 	return $this->fetchAll()->toArray();
	 }
	 /**
	 * Fetch User Resources
	 * Fetches the resources from the Db, for a specified user and returns an array
	 * 
	 * @param int user ID
	 * @return array
	 */
	 public function fetchUserResources($id)
	 {
	 	$select = $this->select()
	 				   ->setIntegrityCheck(false)
	 				   ->from(
		 				   array('r' => 'resources'),
		 				   array('id')
		 				   )
		 			   ->joinInner(
		 			   	   array('ur' => 'user_resources'),
		 			   	   'r.id = ur.resource_id',
		 			   	   array('permission')
			 			   )
				 	   ->where('ur.user_id = ?', $id);
		return $this->fetchAll($select)->toArray();
	 }
}








