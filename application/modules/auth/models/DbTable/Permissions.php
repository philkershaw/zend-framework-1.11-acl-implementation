<?php

class Auth_Model_DbTable_Permissions extends Zend_Db_Table_Abstract
{
	protected $_name = "user_resources";
	/**
	 * Save Permissions
	 * Saves the permissions for the user and adds permissions for newly added resources
	 * 
	 * @param array $data
	 */
	public function savePermissions($data)
	{
		$id = $data['user'];
		unset($data['user']);

		foreach ($data as $resourceID => $permission)
		{
			$row = $this->fetchRow(
				$this->select()
					 ->where('user_id = ?', $id)
					 ->where('resource_id = ?', $resourceID)
				);
			if (!$row)
			{
				$insert = array(
					'user_id' => $id,
					'resource_id' => $resourceID,
					'permission' => $permission
					);
				$this->insert($insert);
			}
			else
			{
				$update = array(
					'permission' => $permission
					);
				$this->update($update, "user_id = {$id} AND resource_id = {$resourceID}");		
			}
		}
	}
	/**
	 * getUserPermissions
	 * Fetches the permission data for a specified user
	 * 
	 * @param int $id
	 * @return array
	 */
	 public function getUserPermissions($id)
	 {
	 	return $this->fetchAll(
		 			$this->select()
		 		 		 ->where('user_id = ?', $id)
		 			)->toArray();
	 }
}