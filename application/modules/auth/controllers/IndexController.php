<?php

class Auth_IndexController extends Zend_Controller_Action
{
    protected $_redirector = null;
    /**
     * Init
     * Sets up a local object for the redirector helper and ansures flash messages are displayed where used within actions.
     */
    public function init()
    {
        $this->_redirector = $this->_helper->getHelper('Redirector');
        $this->view->message = $this->_helper->flashMessenger->getMessages();
    }

    public function indexAction()
    {
        $this->_helper->layout->setLayout('login');
        $form = new Auth_Form_Login();

        if ($this->_request->isPost())
        {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) 
            {
                $salt    = Zend_Registry::get('salt');
                $adapter = $this->_getAuthAdapter()
                                ->setIdentity($post['username'])
                                ->setCredential($post['password'].$salt);
                $auth    = Zend_Auth::getInstance();
                $result  = $auth->authenticate($adapter);
                if (!$result->isValid()) 
                {
                    $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "Username/Password incorrect"));
                    $this->_redirector->gotoUrl('/login');
                }
                else
                {
                    $storage = $auth->getStorage();
                    $storage->write($adapter->getResultRowObject(
                        null,
                        'password'
                    ));
                }
                $this->_redirector->gotoUrl('/');
            }
        }
        $this->view->form = $form;
    }

    public function logoutAction()
    {
        Zend_Auth::getInstance()->clearIdentity();
        $this->_redirector->gotoUrl('/login'); // back to login page
    }
    
    protected function _getAuthAdapter()
    {
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
        
        $authAdapter->setTableName('users')
                    ->setIdentityColumn('username')
                    ->setCredentialColumn('password')
                    ->setCredentialTreatment('SHA1(?)');
            
        return $authAdapter;
    }   
}
?>