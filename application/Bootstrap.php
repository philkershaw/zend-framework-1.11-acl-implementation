<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
  protected function _initViewHelpers()
  {
      $this->bootstrap('view');
      $view = $this->getResource('view');
      $view->addHelperPath(APPLICATION_PATH . '/../library/ZendX/JQuery/View/Helper', 'ZendX_JQuery_View_Helper');
  }
	/**
	 * Setup multiple DB connections
	 */
	public function _initDatabase(){
    $database = new Zend_Session_Namespace('billing');
    $resource = $this->getPluginResource('multidb');
    $resource->init();
    // Custom registry keys for MultiDB objects
    Zend_Registry::set('billing',$resource->getDb('billing'));
  }
  /**
   * Store salt in Zend Registry for later use
   */
  public function _initSalt(){
    Zend_Registry::set('salt', '5KYZSHlKkGllWI+^HN-<FF*wJQLj@@Z6=gH~UynZ1^n3|l5Z}V[B3$+D!3},bAxz');
  }
  /**
   * Init placeholders for the view
   */
  // protected function _initPlaceholders()
  // {
  //     $this->bootstrap('View');
  //     $view = $this->getResource('View');

  //     // Set the initial title and separator:
  //     $view->headTitle('Zimo Billing')
  //          ->setSeparator(' - ');
  // }
  /**
   * Set initial head meta data
   */
  protected function _initHeadMeta(){
    $layout = new Zend_Layout();
    $layout->getView()->headMeta()
                      ->appendName("copyright", "Copyright Zimo Communications Ltd., all rights reserved.");
  }
}