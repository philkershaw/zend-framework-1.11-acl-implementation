<?php

class Admin_Bootstrap extends Zend_Application_Module_Bootstrap
{
	public function _initRouter()
	{
        $router = Zend_Controller_Front::getInstance()->getRouter();

        $router->addRoute('permissions',
        	        new Zend_Controller_Router_Route('admin/permissions/:crud',
	        				array(
	        					'module' 	 => 'admin',
	        					'controller' => 'index',
	        					'action' 	 => 'permissions',
	        					'crud' 		 => null
		        				)
		        			)
	        );
	    $router->addRoute('update-resources',
        	        new Zend_Controller_Router_Route('admin/update-resources',
	        				array(
	        					'module' 	 => 'admin',
	        					'controller' => 'index',
	        					'action' 	 => 'updateresources'
		        				)
		        			)
	        );
	    $router->addRoute('users',
	    		new Zend_Controller_Router_Route('admin/users',
	    				array(
	    						'module' 	 => 'admin',
	    						'controller' => 'users',
	    						'action' 	 => 'index',
	    				)
	    		)
	    );
	    $router->addRoute('user',
        	        new Zend_Controller_Router_Route('admin/user/:id',
	        				array(
	        					'module' 	 => 'admin',
	        					'controller' => 'users',
	        					'action' 	 => 'view',
	        					'id'		 => null
		        				)
		        			)
	        );
	    $router->addRoute('update',
	    		new Zend_Controller_Router_Route('admin/user/update/:id',
	    				array(
	    						'module' 	 => 'admin',
	    						'controller' => 'users',
	    						'action' 	 => 'update',
	    						'id'		 => null
	    				)
	    		)
	    );
	    $router->addRoute('update',
	    		new Zend_Controller_Router_Route('admin/user/add',
	    				array(
	    						'module' 	 => 'admin',
	    						'controller' => 'users',
	    						'action' 	 => 'add',
	    				)
	    		)
	    );
	}
}