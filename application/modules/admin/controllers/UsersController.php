<?php
/**
 * Users Controller
 * Provisiona the adding, editing, deletion and general modification to users and their full profiles
 *
 * @author Phil Kershaw
 */
 class Admin_UsersController extends Zend_Controller_Action
 {
 	protected $_redirector = null;
 	/**
 	 * User table object
 	 * Set up by the constructor to avoid having to perform instanciation within ever action
 	 * 
 	 * @var $userTbl;
 	 */
 	public  $userTbl;
 	/**
 	 * Init Method
 	 * Creates and instance of the DbTable Users Object and stores it in $userTbl class variable
 	 */
 	public function init()
 	{
 		$this->userTbl = new Auth_Model_DbTable_Users();
 		$this->_redirector = $this->_helper->getHelper('Redirector');
 		$this->view->message = $this->_helper->flashMessenger->getMessages();
 	}
 	/**
	 * Index Action
	 * Fetches all users for displaying in a list
 	 */
 	public function indexAction()
 	{
 		$this->view->users = $this->userTbl->getAll();
 	}
 	/**
 	 * Add Action
 	 * Invoked via ajax inserts a new user into the Db and redirects to the view for detail entry
 	 * 
 	 * @param array $_POST
 	 */
 	public function addAction()
 	{
 		$this->_helper->layout()->disableLayout();
 		$this->_helper->viewRenderer->setNoRender(true);
 			
 		if ($this->_request->isXmlHttpRequest())
 		{
 			$post = $this->_request->getPost();
 			$salt = Zend_Registry::get('salt');
 			$password = substr(md5(uniqid($salt, true)), 0, 8);
 			$post['password'] = sha1($password.$salt);
 			try
 			{
 				if ($this->userTbl->add($post))
 				{
 					$this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "User created successfully. Please enter more detail for this user below."));
 					$insertId = $this->userTbl->getAdapter()->lastInsertId();
 					echo "/admin/user/{$insertId}";
 				}
 				else
 				{
 					$this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "User already exists."));
 					echo "/admin/users";
 				}
 			}
 			catch (Exception $e)
 			{
 				throw new Exception("Failed to add user: {$e->getMessage()}");
 			}
 		}
 	}
 	/**
 	 * View Action
 	 * Fetches a single user for displaying
 	 * 
 	 * @param int $id
 	 */
 	public function viewAction()
 	{
 		$this->view->user = $this->userTbl->get($this->_request->getParam('id'));
 	}
 	/**
 	 * Update
 	 * Updates a users record and is invoked via an ajax callback - thus doesn't require a view
 	 * 
 	 * @param int $id
 	 * @param array $_POST
 	 * 
 	 * @return mixed
 	 */
 	public function updateAction()
 	{
 		$this->_helper->layout()->disableLayout();
 		$this->_helper->viewRenderer->setNoRender(true);
 		
 		if ($this->_request->isXmlHttpRequest())
 		{
 			$id = $this->_request->getParam('id');
 			$post = $this->_request->getPost();
 			try
 			{
 				if ($this->userTbl->updateUser($id, $post))
 				{
 					echo 'User updated successfully';
 				}
 				else 
 				{
 					echo 'No changes detected, user was not updated';
 				}
 			}
 			catch (Exception $e)
 			{
 				throw new Exception("Failed to update user: {$e->getMessage()}");
 			}
 		}
 	}
 	/**
	 * Permissions
	 * Displays a form for managing user permissions to resources
	 */
	public function permissionsAction()
	{
        if ($this->_request->isPost())
        {
        	switch ($this->_request->getParam('crud'))
        	{
        		case 'update':
        			$post = $this->_request->getPost();
		            unset($post['save']);
		            $permissionsTbl = new Auth_Model_DbTable_Permissions();
		            try
		            {
		                $permissionsTbl->savePermissions($post);
		                $this->_helper->flashMessenger->addMessage(array('type' => 'success', 'message' => "Permissions updated successfully."));
		            }
		            catch (Exception $e)
		            {
		                $this->_helper->flashMessenger->addMessage(array('type' => 'error', 'message' => "There was a problem saving the permissions. The following error occurred {$e->getMessage()}"));
		            }
		            $this->_redirector->gotoUrl('/admin/permissions');		
        			break;
        		default:
        			$user = $_POST['users'];
        			break;
        	}
        }
        $userForm = new Admin_Form_Users(array(
            'action' => '/admin/permissions',
            'method' => 'post',
            ));
        
        $userTbl = new Auth_Model_DbTable_Users();
		$users = $this->reformatArray($userTbl->getAllUsers());
		if (!isset($user))
		{
			$userID = key($users);	
		}
		else
		{
			$userID = $user;
		}
		$userSelector = $userForm->getElement('users');
		$userSelector->setValue($userID)
				 	 ->setMultiOptions($users);

        $permissionsForm = new Admin_Form_Permissions($userID, array(
            'action' => '/admin/permissions/update',
            'method' => 'post',
            ));
        $this->view->form = $permissionsForm;
        $this->view->users = $userForm;
	}
	/**
	 * Reformat Array
	 * Arranges the array in a format ready for use with a Zend Select element
	 * 
	 * @return array reformatted data
	 */
	private static function reformatArray(Array $users)
	{
		$newArray = array();
		foreach ($users as $user)
		{
			$newArray[$user['id']] = $user['username'];
		}
		return $newArray;
	}
 }