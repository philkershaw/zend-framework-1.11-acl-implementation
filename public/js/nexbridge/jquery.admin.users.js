/**
 * Admin Panel, Users 
 * Provisions the updating of user profiles via AJAX.
 * 
 * Developed with jQuery 1.7.1
 * @author 	Phil Kershaw
 * @date	11 Jan 2011 
 */
$(document).ready(function(){
	$('#save').live('click', function(e){
		e.preventDefault();
		
		id = $('td#id').text();
		username = $('td#username').text();
		role = $('td#role').text();
		
		$.post('/admin/user/update/'+id,
				{username : username, role : role},
				function(data)
				{
					alert(data);
				});
	});
	
	$('#add').live('click', function(e){
		e.preventDefault();
		$(this).attr('disabled', 'disabled');
		username = $('div#username').text();
		
		$.post('/admin/user/add',
				{username : username},
				function(data)
				{
					window.location = data;
				});
	});
});
