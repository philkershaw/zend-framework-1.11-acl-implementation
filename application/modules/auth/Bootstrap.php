<?php

class Auth_Bootstrap extends Zend_Application_Module_Bootstrap
{
    /**
     * Init Router
     * Creates routes
     */
    public function _initRouter(){
        $router = Zend_Controller_Front::getInstance()->getRouter();

        $router->addRoute('login',
                    new Zend_Controller_Router_Route('login',
                            array(
                                'module'     => 'auth',
                                'controller' => 'index',
                                'action'     => 'index'
                                )
                            )
            );
        $router->addRoute('logout',
                    new Zend_Controller_Router_Route('logout',
                            array(
                                'module'     => 'auth',
                                'controller' => 'index',
                                'action'     => 'logout'
                                )
                            )
            );
    }
    /**
     * Init Salt
     * Creates a Salt for use in authentication and adds it to the Zend Registry.
     */
    public function _initSalt(){
            Zend_Registry::set('salt', '9Hi*&nN}5A4jjSD£MzC,<%NNI7}H`@UY0$pC+d?xv,,|Y?z%:<?kW3 _G+Vy,pOss?gjc1');
    }

    public function _initACL()
    {           
        $fc = Zend_Controller_Front::getInstance();
        $fc->registerPlugin(new Zimo_Acl_Plugin());
    }
}

?>
