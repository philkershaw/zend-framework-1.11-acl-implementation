<?php
/**
 * Zimo ACL Resources
 * Complies an array of Modules/Controllers/Actions, adds them as resources.
 * 
 * @author Phil Kershaw
 */
class Zimo_Acl_Resources
{
	/**
	 * A nested array containing all modules/controllers/actions
	 * 
	 * @var array
	 */
	public $resources = array();
	 /**
	 * An array of module names
	 * 
	 * @var array
	 */
	public $modules = array();
	 /**
	 * An array of controller names
	 * 
	 * @var array
	 */
	public $controllers = array();
	/**
	 * Contructor
	 * Calls findModules and findControllers
	 */
	 public function __construct()
	 {
	 	$this->findModules();
	 	$this->findControllers();
	 	$this->buildResources();
	 }
	/**
	 * Build Resource Array
	 * Crawls through the given modules path, finds all modules/controllers/actions and builds an array with their names
	 * 
	 * @return array $this->resourceArray A nested array containing all modules/controllers/actions
	 */
	public function buildResources()
	{
		foreach ($this->controllers as $module => $controllers)
		{
			foreach ($controllers as $controller)
			{
				$class = ucfirst($module).'_'.ucfirst($controller).'Controller';
				if (!class_exists($class))
				{
					Zend_Loader::loadfile(APPLICATION_PATH . "/modules/{$module}/controllers/{$controller}Controller.php");
				}
				$reflection = new Zend_Reflection_Class($class);
				$methods	= $reflection->getMethods();
				$rtn = array();
				foreach ($methods as $method)
				{
					if (preg_match("/(\w+)Action$/", $method->name, $action))
					{
						$this->resources[$module][$controller][] = $this::strhyphen($action[1]);
					}
				}
			}
		}
	}
	/**
	 * Find modules
	 * Crawls the modules folder and puts the names into an array
	 * 
	 * @return array $modules
	 */
	public function findModules()
	{
		$path = APPLICATION_PATH . "/modules";
		if ($handle = opendir($path)) 
		{
		    while (false !== ($module = readdir($handle))) 
		    {
		        if ($module != "." && $module != "..") 
		        {
		            array_push($this->modules, $module);
		        }
		    }
		    closedir($handle);
		}
	}
	/**
	 * Find Controllers
	 * Searches the modules defined in the $modues var for controllers and puts them into an array
	 * 
	 * @param array $modules
	 * @return array $controllers
	 */
	public function findControllers()
	{
	  	foreach ($this->modules as $module)
	  	{
	  		$path = APPLICATION_PATH . "/modules/{$module}/controllers";
			if ($handle = opendir($path)) 
			{
			    while (false !== ($filename = readdir($handle))) 
			    {
			        if (preg_match("/(\w+)Controller\.php$/", $filename, $controller)) 
			        {
			        	$this->controllers[$module][] = strtolower($this::strhyphen($controller[1]));
			        }
			    }
			    closedir($handle);
			}	
	  	}
	}
	/**
	 * strhyphen
	 * Converts camelCase to hyphenated
	 * 
	 * @param string $string the string to convert
	 * @return string $return the converted string
	 */
	private static function strhyphen($string)
	{
		preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $string, $matches);
		$ret = $matches[0];
		foreach ($ret as &$match) {
			$match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
		}
		return implode('-', $ret);
	}
}